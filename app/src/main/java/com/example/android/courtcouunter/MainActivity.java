package com.example.android.courtcouunter;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    int quant = 0, quanta = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        displayForTeamA(0);
        displayForTeamB(0);
    }
    public void reset(View v) {
        quant=0;
        quanta=0;
        displayForTeamA(quant);
        displayForTeamB(quanta);
    }

    public void displayForTeamA(int score) {
        TextView scoreView = (TextView) findViewById(R.id.team_a_score);
        scoreView.setText(String.valueOf(score));
    }

    public void displayForTeamB(int score) {
        TextView scoreView = (TextView) findViewById(R.id.team_b_score);
        scoreView.setText(String.valueOf(score));
    }

    public void add3(View v) {
        quant = quant + 3;
        displayForTeamA(quant);
    }

    public void add2(View v) {
        quant = quant + 2;
        displayForTeamA(quant);
    }

    public void add1(View v) {
        quant = quant + 1;
        displayForTeamA(quant);
    }


    public void adda(View v) {
        quanta = quanta + 3;
        displayForTeamB(quanta);
    }

    public void addb(View v) {
        quanta = quanta + 2;
        displayForTeamB(quanta);
    }

    public void addc(View v) {
        quanta = quanta + 1;
        displayForTeamB(quanta);
    }
}
